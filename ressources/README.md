# Gestion Hébergement AFPAR

## Ne pas oublier de synchronizer son projet a chaque lencement, modification ou commit des membres du groupe ## 

## Installation  !!!IMPORTANT!! A LIRE IMPERATIVEMENT  ##

## COUCOU DIMITRI

### Procédure de commit sur GitLab :

 1. Vérifier la Branche sur laquelle vous aller commit et push (En bas à gauche de l'écran dans la barre bleu l'icône branche "Exemple Branche>Main") 
 2. Vérifier que le code modifié est bien sauvegardé "CTRL + S" 
 3. Cliquer sur l'onglet à gauche avec l'icône Branche "Contrôle de code source" X Changement en attente 
 4. Cliquer sur Commit (Bouton Bleu dans la barre) 
 5. Dans l'onglet ouvert, entrer le message du commit à la fin. OBLIGATIORE sinon le commit sera impossible !!!
 6. Valider le commit en haut à droite de l'écran avec la checkMark 
 7. Cliquer sur synchroniser "Push" (Bouton bleu dans la barre) 
 8. Vérifier qu'il n'y a plus de "M" à côté du ficher 


## Ajoute de nouveaux dossiers

Push un dépôt Git existant avec la commande suivante :

```
cd existing_repo
git remote add origin https://gitlab.com/dwwm_gr1/gestion-hebergement-afpar.git
git branch -M main
git push -uf origin main
```

## Nom
Choisissez un nom explicite pour votre projet.

## La description
Faites savoir aux gens ce que votre projet peut faire spécifiquement. Fournissez un contexte et ajoutez un lien vers toute référence que les visiteurs pourraient ne pas connaître. Une liste de fonctionnalités ou une sous-section Contexte peut également être ajoutée ici. S'il existe des alternatives à votre projet, c'est un bon endroit pour lister les facteurs de différenciation.

## Visuels
Selon ce que vous faites, il peut être judicieux d'inclure des captures d'écran ou même une vidéo (vous verrez fréquemment des GIF plutôt que des vidéos réelles). Des outils comme ttygif peuvent aider, mais consultez Asciinema pour une méthode plus sophistiquée.

## Utilisation
Utilisez généreusement des exemples et montrez le résultat attendu si vous le pouvez. Il est utile d'avoir en ligne le plus petit exemple d'utilisation que vous pouvez démontrer, tout en fournissant des liens vers des exemples plus sophistiqués s'ils sont trop longs pour être raisonnablement inclus dans le README.

## Auteurs
Brian Wood
Dimitri Lerandy
Jean Bertrand Marie-Marthe
Christophe Buchoux

## Licence
Pour les projets open source, indiquez la licence.

## L'état du projet
En Préparation