

var ctx = document.getElementById('graph1').getContext('2d')

        var data = {
          labels: ["Occupé","Libre"],
          datasets: [{
            data:[24,32],
            backgroundColor: ['#8f8c8e', '#008c40'],
          }]
        }

        var options = {
            responsive: true
        }
        
        var config = {
            type: 'pie',
            data: data,
            options: options,
            
        }
        var graph1 = new Chart(ctx, config)

  var $table = $('#table')

  $(function() {
    $('#toolbar').find('select').change(function () {
      $table.bootstrapTable('destroy').bootstrapTable({
        exportDataType: $(this).val(),
        exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel', 'pdf'],
        columns: [
          {
            field: 'state',
            checkbox: true,
            visible: $(this).val() === 'selected'
          },
          {
            field: 'nom_prenom',
            title: 'Nom Prenom'
          }, {
            field: 'tel',
            title: 'Tel'
          }, {
            field: 'genre',
            title: 'Genre'
          }, {
            field: 'session',
            title: 'Session'
          }
          , {
            field: 'chambre',
            title: 'Chambre'
          }, {
            field: 'periode_de_formation',
            title: 'Periode de Formation'
          }, {
            field: 'cle',
            title: 'Cle'
          }
        ]
      })
    }).trigger('change')
  })

  document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    themeSystem: 'bootstrap5',
    initialView: 'dayGridMonth',
    locale:'fr',
    initialDate: '2022-11-07',
    headerToolbar: {
      start: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    events: [
      {
        title: 'Ping-Pong',
        start: '2022-11-01'
      },
      {
        title: 'Sorti Piscine',
        start: '2022-11-07',
        end: '2022-11-10'
      },
    ]
  });

  calendar.render();
});

var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})

var modalMention = document.getElementById('modalMention')
var inputMention = document.getElementById('inputMention')

var modalContact = document.getElementById('modalContact')
var inputContact = document.getElementById('inputContact')

var modalPlan = document.getElementById('modalPlan')
var inputPlan = document.getElementById('inputPlan')

var modaladdevent = document.getElementById('modaladdevent')
var inputaddevent = document.getElementById('inputaddevent')

var modaldelevent = document.getElementById('modaldelevent')
var inputdelevent = document.getElementById('inputdelevent')

modalMention.addEventListener('shown.bs.modal', function () {
  inputMention.focus()
})

modalContact.addEventListener('shown.bs.modal', function () {
  inputContact.focus()
})

modalPlan.addEventListener('shown.bs.modal', function () {
  inputPlan.focus()
})

modaladdevent.addEventListener('shown.bs.modal', function () {
  inputaddevent.focus()
})

modaldelevent.addEventListener('shown.bs.modal', function () {
  inputdelevent.focus()
})

