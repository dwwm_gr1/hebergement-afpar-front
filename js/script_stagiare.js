  document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    themeSystem: 'bootstrap5',
    initialView: 'dayGridMonth',
    locale:'fr',
    initialDate: '2022-11-07',
    headerToolbar: {
      start: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    events: [
      {
        title: 'Ping-Pong',
        start: '2022-11-01'
      },
      {
        title: 'Sorti Piscine',
        start: '2022-11-07',
        end: '2022-11-10'
      },
    ]
  });

  calendar.render();
});

var $table = $('#table')

$(function() {
  $('#toolbar').find('select').change(function () {
    $table.bootstrapTable('destroy').bootstrapTable({
    })
  }).trigger('change')
})
